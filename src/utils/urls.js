export const HOME_PAGE = "/"
export const ABOUT_PAGE = "/about"
export const CONTACTS_PAGE = "/contacts"
export const SKILLS_PAGE = "/skills"
export const PORTFOLIO_PAGE = "/portfolio"
export const LOGIN_PAGE = "/login"
export const REGISTER_PAGE = "/register"
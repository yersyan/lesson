import React, {useState} from 'react';
import Button from "../ui/button";
import css from "./skills.module.scss"
import TextField from "../ui/input";

const Skills = () => {

    const [skills, setSkills] = useState([
        {id: Math.random(), name: "HTML"},
        {id: Math.random(), name: "CSS"},
        {id: Math.random(), name: "JS"},
        {id: Math.random(), name: "REACT JS"},
    ])

    const [skillName, setSkillName] = useState('')

    const addSkill = () => {
        if (skillName.trim()){
            setSkills(skills => skills.concat([{id: Math.random(), name: skillName}]))
            setSkillName('')
        }
    }

    const deleteSkill = (id) => {
        setSkills(skills => skills.filter(s => s.id !== id))
    }

    const [editRegime, setEditRegime] = useState({})
    const [editText, setEditText] = useState({})

    // mtnum enq editi regim
    const changeRegime = (id, name) => {
        setEditRegime({[id]: true})
        setEditText({[id]: name})
    }

    const saveSkill = (id) => {
        skills.forEach(s => {
            if (s.id === id){
                s.name = editText[id]
            }
        })
    }


    return (
        <div className={css.skills}>
            <ul>
                {
                    skills.map(({id, name}) => {
                        return <li key={id}>
                            {
                                editRegime[id]
                                    ? <TextField
                                        className="inputSkills"
                                        value={editText[id]}
                                        onChange={e => setEditText({[id]: e.target.value})}
                                    />
                                    : <span>{name}</span>
                            }
                            <div>
                                <Button
                                    className="buttonSkills"
                                    onClick={
                                        editRegime[id]
                                            ? () => {
                                                saveSkill(id)
                                                setEditRegime({})
                                            }
                                            :  () => changeRegime(id, name)
                                    }
                                >
                                    {editRegime[id] ? 'Save' : 'Edit'}
                                </Button>
                                <Button
                                    className="buttonSkills"
                                    onClick={() => deleteSkill(id)}
                                >
                                    Delete
                                </Button>
                            </div>
                        </li>
                    })
                }
            </ul>
            <div className={css.addForm}>
                <TextField
                    className="inputSkills"
                    value={skillName}
                    onChange={e => setSkillName(e.target.value)}
                />
                <Button
                    className="buttonSkills"
                    onClick={addSkill}
                >
                    Add
                </Button>
            </div>
        </div>
    );
};

export default Skills;
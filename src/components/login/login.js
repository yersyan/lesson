import React, {useState} from 'react';
import Form from "../ui/form";
import Button from "../ui/button";
import css from "./login.module.scss"
import TextField from "../ui/input";

const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onChangeEmail = (e) => {
        setEmail(e.target.value)
    }
    const onChangePassword = (e) => {
        setPassword(e.target.value)
    }

    const login = (e) => {
        e.preventDefault()
        let localEmail = JSON.parse(localStorage.getItem('user'))['email']
        let localPassword = JSON.parse(localStorage.getItem('user'))['password']
        if(localEmail === email && localPassword === password){
            localStorage.setItem('token', 'hjghfkjghaskjghasd1212341gdfg')
            window.location.reload()
        }
    }

    return (
        <div className={css.login}>
            <Form onSubmit={login}>
                <TextField
                    title="email"
                    type="email"
                    placeholder="email"
                    value={email}
                    onChange={onChangeEmail}
                />
                <TextField
                    title="password"
                    type="password"
                    placeholder="password"
                    value={password}
                    onChange={onChangePassword}
                />
                <Button
                    title="Login"
                    className="button"
                />
            </Form>
        </div>
    );
};

export default Login;
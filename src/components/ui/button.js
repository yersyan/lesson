import React from 'react';
import css from "./ui.module.scss"

const Button = ({children, className, ...props}) => {

    return (
        <button className={css[className]} {...props}>
            {children}
        </button>
    );
};

export default Button;
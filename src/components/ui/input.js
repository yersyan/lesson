import React from 'react';
import css from './ui.module.scss'

const TextField = ({title, className, ...props}) => {

    return (
        <label className={css[className]}>
            <div>{title}</div>
            <input {...props}/>
        </label>
    );
};

export default TextField;
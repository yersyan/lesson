import React from 'react';
import  css from './ui.module.scss'

const Form = ({children, ...props}) => {

    return (
        <form className={css.form} {...props}>
            {children}
        </form>
    );
};

export default Form;
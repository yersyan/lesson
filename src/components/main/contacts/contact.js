import React from 'react';
import css from './contacts.module.scss'

const Contact = ({contact, value}) => {

    return (
        <div className={css.contact}>
            <span>{contact}</span>
            <span>{value}</span>
        </div>
    );
};

export default Contact;
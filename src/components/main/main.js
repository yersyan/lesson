import React from 'react';
import './main.css'
import Contact from "./contacts/contact";
import Portfolio from "../portfolio/portfolio";
import Pages from "../pages/pages";

const Main = () => {

    const contacts = [
        {id: Math.random(), contact: 'Email', value: 'user@mail.ru'},
        {id: Math.random(), contact: 'Phone', value: '45465453'},
        {id: Math.random(), contact: 'Address', value: 'str.jkjj'},
        {id: Math.random(), contact: 'Fax', value: '44545545'},
    ]


    return (
        <main className="main">
            {/*{*/}
            {/*    contacts.map(({id, contact, value}) => {*/}
            {/*        return <Contact contact={contact} value={value} key={id}/>*/}
            {/*    })*/}
            {/*}*/}
            <Pages/>
        </main>
    );
};

export default Main;
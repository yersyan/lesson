import React, {useState} from 'react';
import css from "./portfolio.module.scss"

const Portfolio = () => {
    const info = [
        {id:1, name:"project1",value:"shop1"},
        {id:2, name:"project2",value:"shop2"},
        {id:3, name:"project3",value:"shop3"},
        {id:4, name:"project4",value:"shop4"}
    ];

    const showPortfolio = useState()

    const [type, setType] = useState({})

    // const showValue = (id) => {
    //     setType({ [id]: !type[id]})
    // }

    const showValue = (id) => {
        setType(prevState => ({
            ...prevState,
            [id]: !type[id]
        }))
    }



    return (
        <div>
            <ul className={css.portfolio}>
                {
                    info.map(({id, name, value}) => {
                       return <li key={id}>
                           <span
                               onClick={() => showValue(id)}
                           >
                               {name}
                           </span>
                           {type[id] && <span>{value}</span>}
                       </li>
                    })
                }
            </ul>
        </div>
    );
};

export default Portfolio;
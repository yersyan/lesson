import React from "react";
import css from './header.module.scss'
import Logo from "./logo/logo";
import Menu from "./menu/menu";
import Button from "../ui/button";



const Header = () => {

    return <header
        className={css.header}
        // style={{backgroundColor: "red"}}
    >
       <Logo/>
      <Menu/>

        <Button
            className="buttonLogin"
            onClick={() => {
                localStorage.removeItem('token')
                window.location.reload()
            }}
        >
            Logout
        </Button>
    </header>
}

export default Header
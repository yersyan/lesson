import React, {useState} from 'react';
import css from "./menu.module.scss"
import {AiOutlineMenu} from "react-icons/ai";
import {authRoutes, routes} from "../../pages/routes";
import {Link, useLocation, useNavigate} from "react-router-dom";

const Menu = () => {
    const isAuth = localStorage.getItem('token')

    const [showMenu, setShowMenu] = useState(false)

    const {pathname} = useLocation()


    return (
        <div>
            <ul className={showMenu ? css.block : css.menu}>
                {
                    isAuth
                        ? authRoutes.map(({path, name}) => {
                            return <li key={path}>
                                <Link
                                    className={path === pathname ? css.active : css.link}
                                    to={path}
                                    exact='true'
                                >
                                    {name}
                                </Link>
                            </li>
                        })
                        : routes.map(({path, name}) => {
                            return <li key={path}>
                                <Link
                                    className={path === pathname ? css.active : css.link}
                                    to={path}
                                    exact='true'
                                >
                                    {name}
                                </Link>
                            </li>
                        })
                }
            </ul>
            <div className={css.icon} onClick={() => setShowMenu(!showMenu)}>
                <AiOutlineMenu/>
            </div>
        </div>
    );
};

export default Menu;
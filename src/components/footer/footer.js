import React from 'react';
import './footer.css'

const Footer = () => {
    const date = new Date().getFullYear()

    return (
        <footer className="footer">
            &copy;copyright {date}
        </footer>
    );
};

export default Footer;
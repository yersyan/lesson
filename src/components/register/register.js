import React, {useState} from 'react';
import Form from "../ui/form";
import Button from "../ui/button";
import css from './register.module.scss'
import TextField from "../ui/input";
import {useNavigate} from "react-router-dom";
import {LOGIN_PAGE} from "../../utils/urls";

const Register = () => {
     const navigate = useNavigate()

    const[username, setUsername] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');

    const onChangeUsername = (e) => {
        setUsername(e.target.value)
    }
    const onChangeEmail = (e) => {
        setEmail(e.target.value)
    }
    const onChangePassword = (e) => {
        setPassword(e.target.value)
    }

    console.log(username, email, password)

    const register = (e) => {
        e.preventDefault()
        localStorage.setItem('user', JSON.stringify({
            username,
            email,
            password
        }))
        navigate(LOGIN_PAGE)
    }

    return (
        <div className={css.register}>
            <Form onSubmit={register}>
                <TextField
                    title="username"
                    type="text"
                    placeholder="username"
                    value={username}
                    onChange={onChangeUsername}
                />
                <TextField
                    title="email"
                    type="email"
                    placeholder="email"
                    value={email}
                    onChange={onChangeEmail}
                />
                <TextField
                    title="password"
                    type="password"
                    placeholder="password"
                    value={password}
                    onChange={onChangePassword}
                />
                <Button
                    title="Login"
                    className="button"
                />
            </Form>
        </div>
    );
};

export default Register;
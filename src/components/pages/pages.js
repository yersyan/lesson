import React from 'react';
import {Navigate, Route, Routes} from "react-router-dom";
import {authRoutes, routes} from "./routes";
import {HOME_PAGE, LOGIN_PAGE} from "../../utils/urls";

const Pages = () => {
    const isAuth = localStorage.getItem('token')

    return (
        <Routes>
            {
                isAuth
                    ? authRoutes.map(({path, element}) => {
                        return <Route
                            key={path}
                            path={path}
                            element={element}
                            exact='true'
                        />
                    })
                    : routes.map(({path, element}) => {
                        return <Route
                            key={path}
                            path={path}
                            element={element}
                            exact='true'
                        />
                    })
            }
            <Route
                path={'*'}
                element={<Navigate to={isAuth ? HOME_PAGE : LOGIN_PAGE}/>}
            />
        </Routes>
    );
};

export default Pages;
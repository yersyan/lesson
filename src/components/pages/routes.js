import {
    ABOUT_PAGE,
    CONTACTS_PAGE,
    HOME_PAGE,
    LOGIN_PAGE,
    PORTFOLIO_PAGE,
    REGISTER_PAGE,
    SKILLS_PAGE
} from "../../utils/urls";
import Home from "../home/home";
import About from "../about/about";
import Contacts from "../contacts/contacts";
import Portfolio from "../portfolio/portfolio";
import Skills from "../skills/skills";
import Login from "../login/login";
import Register from "../register/register";

export const authRoutes = [
    {
        path: HOME_PAGE,
        element: <Home/>,
        name: 'Home'
    },
    {
        path: PORTFOLIO_PAGE,
        element: <Portfolio/>,
        name: 'Portfolio'
    },
    {
        path: ABOUT_PAGE,
        element: <About/>,
        name: 'About'
    },
    {
        path: CONTACTS_PAGE,
        element: <Contacts/>,
        name: 'Contacts'
    },
    {
        path: SKILLS_PAGE,
        element: <Skills/>,
        name: 'Skills'
    },
]

export const routes = [
    {
        path: LOGIN_PAGE,
        element: <Login/>,
        name: 'Login'
    },
    {
        path: REGISTER_PAGE,
        element: <Register/>,
        name: 'Register'
    },
]